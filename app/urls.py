from django.urls import path
from app.views import HotelView

urlpatterns = [
    path('index/', HotelView.as_view(), name='index'),
]