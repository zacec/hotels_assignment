from django.db import models


class Hotel(models.Model):
    city = models.ForeignKey('City', on_delete=models.CASCADE)
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class City(models.Model):
    code = models.CharField(max_length=30, unique=True)
    name = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.code
