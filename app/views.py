from django.http import JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from .models import City, Hotel
from .utils import get_next_free_hotel_id


@method_decorator(csrf_exempt, name='dispatch')
class HotelView(View):
    template = "index.html"

    def get(self, request):
        cities = []
        [cities.append(c['name']) for c in City.objects.values('name').distinct()]
        hotels = Hotel.objects.select_related('city').values()
        context = {'hotels': hotels, 'cities': cities}
        return render(request, self.template, context=context)

    def post(self, request):
        action = request.POST.get('action', None)

        if action == 'update_city':
            city_name = request.POST.get('city', None)
            cities_list = []
            [cities_list.append(c['name']) for c in City.objects.values('name').distinct()]
            city = City.objects.filter(name=city_name).values()
            hotels = Hotel.objects.select_related('city').filter(city__code=city[0]['code']).values()
            context = {'hotels': list(hotels), 'cities': cities_list, 'city': list(city)}
            return render(request, self.template, context=context)

        elif action == 'add_hotel':
            city = request.POST.get('city', None)
            hotel_name = request.POST.get('hotel_name', None)

            city_code = City.objects.filter(name=city).values('code')[0]['code']
            hotel_code = get_next_free_hotel_id(city)
            city_obj = City.objects.get_or_create(code=city_code)[0]
            try:
                Hotel.objects.get_or_create(
                    city=city_obj,
                    code=hotel_code,
                    name=hotel_name,
                )
            except ValueError:
                pass

            cities_list = []
            [cities_list.append(c['name']) for c in City.objects.values('name').distinct()]
            city = City.objects.filter(name=city).values()
            hotels = Hotel.objects.select_related('city').filter(city__code=city_code).values()
            context = {'hotels': list(hotels), 'cities': cities_list, 'city': list(city)}
            return render(request, self.template, context=context)

        elif action == 'delete_hotel':
            hotel_code = request.POST.get('hotel_code', None)
            hotel = Hotel.objects.get(code=hotel_code)
            hotel.delete()
            return JsonResponse({'success': True})
        else:
            return JsonResponse({'success': False})
